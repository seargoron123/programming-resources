﻿function Get-EndOfDay {
[CmdletBinding()]
    param(
        [Parameter(Mandatory=$false)]
        [ValidateSet("12hr","24hr")]
        $format = "12hr",
        [Parameter(Mandatory=$false)]
        [string]
        $dayStartInput,
        [Parameter(Mandatory=$false)]
        [string]
        $breakInput,
        [Parameter(Mandatory=$false)]
        [string]
        $currentOTInput
    )
    begin {
        [double]$dayStart = $null
        $breaks = @()
        [double]$breakTotal = 0
        [double]$currentOT = 0
        [string]$EOD
    }
    process {
        if ($null -eq $dayStartInput -or "" -eq $dayStartInput){
            Write-Host -ForegroundColor Green "When did you start your day? (00:00 AM | 00:00)"
            $dayStartInput = Read-Host
        }
        $dayStart = Convert-TimeToDecimal $dayStartInput
        <#
        while ($breakInput -notlike "-1" -and $breakInput -ne -1 -and $breakInput -ne -1.0){
            #Write-Host "Current Breaks: $($breaks)"
            $breakInput = Read-Host "Please enter a break amount or -1 to exit (00:00 | 0.00) "
            if ($breakInput -like "*:*" -or [int]$breakInput -gt 0){
                $breakInput = Convert-TimeToDecimal $breakInput
            }elseif ([double]$breakInput -le 0){
            }else{
                throw "Invalid break input: $($breakInput)"
            }
            if ([int]$breakInput -gt 0){
                $breaks += $breakInput
            }
        }
        foreach ($break in $breaks){
            $breakTotal += $break
        }
        #>
        if ($null -eq $breakInput -or "" -eq $breakInput){
            Write-Host -ForegroundColor Green "Please enter a break amount (00:00 | 0.00)"
            $breakInput = Read-Host
        }
        $breakTotal = Convert-TimeToDecimal $breakInput
        if ($null -eq $currentOTInput -or "" -eq $currentOTInput){
            Write-Host -ForegroundColor Green "How much overtime did you have at the end of yesterday? (00:00 | 0.00)"
            $currentOTInput = Read-Host
        }
        $currentOT = Convert-TimeToDecimal $currentOTInput
        $finalDecimal = $dayStart + $breakTotal - $currentOT + 8
        $EOD = Convert-DecimalToTime -Decimal $finalDecimal -format $format
    }
    end{
        Write-Host ""
        return $EOD
        pause
    }
}
function Convert-TimeToDecimal{
[CmdletBinding()]
    param(
        [string]$Time
    )
    begin {
        [int]$Hours = $null
        [int]$Minutes = $null
        [double]$Decimal = $null
    }
    process {
        if ($Time -like "*PM" -or $Time -like "*AM"){
            if ($Time -like "*:*"){
                $Hours = [int](($Time -split(":"))[0])
                if ($Hours -gt 23){throw "Maximum allowed hours in a day: 23`nReceived: $($Hours)"}
                $Minutes = [int](((($Time -split(":"))[1]) -split " ")[0])
                $Half = [string](((($Time -split(":"))[1]) -split " ")[1])
                if ($Half -like "*PM*"){
                    $Hours += 12
                }
            }
        }elseif ($Time -like "*:*"){
            $Hours = ($Time -split ":")[0]
            $Minutes = ($Time -split ":")[1]
            $Half = ""
        }else{
            $firstDecimal = [double]$Time
            [int]$Hours = [Math]::Floor($firstDecimal)
            if (($firstDecimal - $Hours) -ne 0){
                [int]$Minutes = ($firstDecimal - $Hours)*60
            }else{
                $Hours = 0
                $Minutes = [int]$Time
            }
            $Half = ""
        }
        $Decimal = $Hours + ($Minutes/60)
    }
    end {
        Write-Verbose "Hours: $((($Time -split(":"))[0]))"
        Write-Verbose "Minutes: $((((($Time -split(":"))[1]) -split " ")[0]))"
        Write-Verbose "Half: $($Half)"
        Write-Verbose "Decimal: $($Hours + ($Minutes/60))"
        return $Decimal
    }
}
function Convert-DecimalToTime{
[CmdletBinding()]
    param(
        [double]$Decimal,
        [Parameter(Mandatory=$false)]
        [ValidateSet("12hr","24hr")]
        $format = "12hr"
    )
    begin {
        [int]$Hours = [Math]::Floor($Decimal)
        if ($Hours -gt 23){throw "Maximum allowed hours in a day: 23`nReceived: $($Hours)"}
        [int]$Minutes = ($Decimal - $Hours)*60
        [string]$Half = "AM"
        [string]$Time = ""
    }
    process{
        if ($format -like "12hr"){
            if ($Hours -le 12){
                $Half = "AM"
            }elseif ($Hours -gt 12){
                $Hours -= 12
                $Half = "PM"
            }
            if ("$([string]$Minutes)".Length -eq 1){
                [string]$Minutes = "0" + "$([string]$Minutes)"
            }
            [string]$Time += [string]$Hours + ":" + [string]$Minutes
            $Time += " " + $Half
        }elseif ($format -like "24hr"){
            if ("$([string]$Minutes)".Length -eq 1){
                [string]$Minutes = "0" + "$([string]$Minutes)"
            }
            $Time += [string]$Hours + ":" + [string]$Minutes
        }
    }
    end {
        return $Time
    }
}